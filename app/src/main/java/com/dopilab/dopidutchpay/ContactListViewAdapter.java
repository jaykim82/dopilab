package com.dopilab.dopidutchpay;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 지엽 on 2016-10-14.
 */

public class ContactListViewAdapter extends BaseAdapter {
    private Context mContext;
    private List<ContactListItem> mItems = new ArrayList<ContactListItem>();

    public ContactListViewAdapter(Context context) {
        mContext = context;
    }

    public ContactListViewAdapter(Context context, List<ContactListItem> lit) {
        mContext = context;
        mItems = lit;
    }

    public void addListItems(ContactListItem item) {
        mItems.add(item);
    }

    public void setListItems(List<ContactListItem> lit) {
        mItems = lit;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public boolean areAllItemsSelectable() {
        return false;
    }

    public boolean isSelectable(int position) {
        try {
            return mItems.get(position).isSelectable();
        } catch (IndexOutOfBoundsException ex) {
            return false;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ContactListItemView itemView;

        if(convertView == null) {
            itemView = new ContactListItemView(mContext, mItems.get(position));
        } else {
            itemView = (ContactListItemView) convertView;
        }

        itemView.setText(0, mItems.get(position).getData(0));
        itemView.setText(1, mItems.get(position).getData(1));

        return itemView;
    }


}
