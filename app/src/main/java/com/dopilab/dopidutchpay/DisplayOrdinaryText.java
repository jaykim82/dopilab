package com.dopilab.dopidutchpay;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by 지엽 on 2016-11-02.
 */

public class DisplayOrdinaryText extends AppCompatActivity {
    TextView mTextView1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_displaytext);

        Button button1 = (Button) findViewById(R.id.another_btn1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity();
            }
        });

        mTextView1 = (TextView) findViewById(R.id.another_tv1);

        Intent intent = getIntent();
        String message = intent.getStringExtra("message");
        String phone = intent.getStringExtra("phone");
        String transferer = intent.getStringExtra("transferer");
        String transferAmount = intent.getStringExtra("transferAmount");

        displayText(message, phone, transferer, transferAmount);

    }

    private void displayText(String message, String phone, String transferer, String transferAmount) {
        mTextView1.setText("Phone Number : " + phone + "\nMessage :\n" + message + "\nTransferer : " + transferer + "\nAmount : " + transferAmount);
    }


    public void closeActivity() {
        Intent resultIntent = new Intent();
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }


}
