package com.dopilab.dopidutchpay;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import java.util.Date;

/**
 * Created by 지엽 on 2016-11-01.
 */

public class TextMessageReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.provider.Telephony.SMS_RECEIVED".equals(intent.getAction())) { // SMS 수신

// SMS 메시지를 파싱합니다.
            Bundle bundle = intent.getExtras();
            Object messages[] = (Object[])bundle.get("pdus");
            SmsMessage smsMessage[] = new SmsMessage[messages.length];

            for(int i = 0; i < messages.length; i++) {
                // PDU 포맷으로 되어 있는 메시지를 복원합니다.
                smsMessage[i] = SmsMessage.createFromPdu((byte[])messages[i], bundle.getString("format"));
            }

            Date curDate = new Date(smsMessage[0].getTimestampMillis());    // SMS 수신 시간 확인
            String phoneNumber = smsMessage[0].getOriginatingAddress();      // SMS 발신 번호 확인
            String message = smsMessage[0].getMessageBody().toString();     // SMS 메시지 확인

            HandleReceivedMessage handler = new HandleReceivedMessage();

            if(handler.identifyTextMessageBank(phoneNumber, message)) {     // 대상 SMS 번호 또는 유형인지 확인

                handler.parseTextMessage(phoneNumber, message);
                CommonUtils.createDepositNotification(context, handler.getTransferer(), handler.getTransferAmount(), phoneNumber, message);

            }


        }

    }

}
