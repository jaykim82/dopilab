package com.dopilab.dopidutchpay;

import android.view.View;
import android.widget.AdapterView;

/**
 * Created by 지엽 on 2016-10-14.
 */

public interface OnDataSelectionListener {
    public void onDataSelected(AdapterView parent, View v, int position, long id);
}
