package com.dopilab.dopidutchpay;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by 지엽 on 2016-10-15.
 */

public class CommonUtils {
    public static final int HANGUL_BEGIN_UNICODE = 44032; // 가
    public static final int HANGUL_END_UNICODE = 55203; // 힣
    public static final int HANGUL_BASE_UNIT = 588;

    public static final int[] INITIAL_SOUND_UNICODE = { 12593, 12594, 12596,
            12599, 12600, 12601, 12609, 12610, 12611, 12613, 12614, 12615,
            12616, 12617, 12618, 12619, 12620, 12621, 12622 };

    public static final char[] INITIAL_SOUND = { 'ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ', 'ㄹ',
            'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅉ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ' };

// ************************************* 한글 유틸 시작 ********************************************
    private static String toHexString(int decimal) {
        Long intDec = Long.valueOf(decimal);
        return Long.toHexString(intDec);
    }

    /**
     * 문자를 유니코드(10진수)로 변환 후 반환 한다.
     *
     * @param ch
     * @return
     */
    public static int convertCharToUnicode(char ch) {
        return (int) ch;
    }

    /**
     * 문자열을 유니코드(10진수)로 변환 후 반환 한다.
     *
     * @param str
     * @return
     */
    public static int[] convertStringToUnicode(String str) {

        int[] unicodeList = null;

        if (str != null) {
            unicodeList = new int[str.length()];
            for (int i = 0; i < str.length(); i++) {
                unicodeList[i] = convertCharToUnicode(str.charAt(i));
            }
        }

        return unicodeList;
    }

    /**
     * 유니코드(16진수)를 문자로 변환 후 반환 한다.
     *
     * @param hexUnicode
     * @return
     */
    public static char convertUnicodeToChar(String hexUnicode) {
        return (char) Integer.parseInt(hexUnicode, 16);
    }

    /**
     * 유니코드(10진수)를 문자로 변환 후 반환 한다.
     *
     * @param unicode
     * @return
     */
    public static char convertUnicodeToChar(int unicode) {
        return convertUnicodeToChar(toHexString(unicode));
    }

    /**
     *
     * @param value
     * @return
     */
    public static String getHangulInitialSound(String value) {

        StringBuffer result = new StringBuffer();

        int[] unicodeList = convertStringToUnicode(value);
        for (int unicode : unicodeList) {

            if (HANGUL_BEGIN_UNICODE <= unicode
                    && unicode <= HANGUL_END_UNICODE) {
                int tmp = (unicode - HANGUL_BEGIN_UNICODE);
                int index = tmp / HANGUL_BASE_UNIT;
                result.append(INITIAL_SOUND[index]);
            } else {
                result.append(convertUnicodeToChar(unicode));

            }
        }

        return result.toString();
    }

    public static boolean[] getIsChoSungList(String name) {
        if (name == null) {
            return null;
        }

        boolean[] choList = new boolean[name.length()];

        for (int i = 0; i < name.length(); i++) {
            char c = name.charAt(i);

            boolean isCho = false;
            for (char cho : INITIAL_SOUND) {
                if (c == cho) {
                    isCho = true;
                    break;
                }
            }

            choList[i] = isCho;

        }

        return choList;
    }

    public static String getHangulInitialSound(String value, String searchKeyword) {
        return getHangulInitialSound(value, getIsChoSungList(searchKeyword));
    }

    public static String getHangulInitialSound(String value, boolean[] isChoList) {

        StringBuffer result = new StringBuffer();

        int[] unicodeList = convertStringToUnicode(value);
        for (int idx = 0; idx < unicodeList.length; idx++) {
            int unicode = unicodeList[idx];

            if (isChoList != null && idx <= (isChoList.length - 1)) {
                if (isChoList[idx]) {
                    if (HANGUL_BEGIN_UNICODE <= unicode
                            && unicode <= HANGUL_END_UNICODE) {
                        int tmp = (unicode - HANGUL_BEGIN_UNICODE);
                        int index = tmp / HANGUL_BASE_UNIT;
                        result.append(INITIAL_SOUND[index]);
                    } else {
                        result.append(convertUnicodeToChar(unicode));
                    }
                } else {
                    result.append(convertUnicodeToChar(unicode));
                }
            } else {
                result.append(convertUnicodeToChar(unicode));
            }
        }

        return result.toString();
    }

// ************************************* 한글 유틸 끝 **********************************************

    public static void createDepositNotification(Context context, String transferer, String transferAmount, String add1, String add2) {
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent newIntent = new Intent(context, DisplayOrdinaryText.class);
        newIntent.putExtra("transferer", transferer);
        newIntent.putExtra("transferAmount", transferAmount);
        newIntent.putExtra("add1", add1);
        newIntent.putExtra("add2", add2);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, newIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder notiBuilder = new Notification.Builder(context);
        notiBuilder.setSmallIcon(R.mipmap.ic_launcher);
//                notiBuilder.setTicker("DoPiLab Alarm!!"); //잠시 표시될 push 제목
        notiBuilder.setWhen(System.currentTimeMillis());
//                notiBuilder.setNumber(10);    //push message 적체 갯수
        notiBuilder.setContentTitle("DoPiLab Alarm!!");  //일반 style push 제목
        notiBuilder.setContentText("Received DoPiLab Application Alarm!!"); //일반 style push 내용
        notiBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
        notiBuilder.setContentIntent(pendingIntent);
        notiBuilder.setAutoCancel(true);
//                notiBuilder.addAction(Action.)

        Notification.BigTextStyle style = new Notification.BigTextStyle();
//                style.setSummaryText("and More +");
        style.setBigContentTitle("DoPiLab Alarm!!");
        style.bigText("Transferer : " + transferer + "\nAmount : " + transferAmount);

        notiBuilder.setStyle(style);

        nm.notify(CommonConstants.PRIORITY1, notiBuilder.build());

    }

}
