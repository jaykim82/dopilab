package com.dopilab.dopidutchpay;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

/**
 * Created by 지엽 on 2016-10-14.
 */

public class ContactListView extends ListView {

    private ContactListViewAdapter adapter;
    private OnDataSelectionListener selectionListener;

    public ContactListView(Context context) {
        super(context);

        init();
    }

    public ContactListView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    private void init() {
        setOnItemClickListener(new OnItemClickAdapter());
    }

    public void setAdapter(BaseAdapter adapter) {
        super.setAdapter(adapter);

    }

    public BaseAdapter getAdapter() {
        return (BaseAdapter)super.getAdapter();
    }

    public void setOnDataSelectionListener(OnDataSelectionListener listener) {
        this.selectionListener = listener;
    }

    public OnDataSelectionListener getOnDataSelectionListener() {
        return selectionListener;
    }

    class OnItemClickAdapter implements OnItemClickListener {
        public OnItemClickAdapter() {

        }

        public void onItemClick(AdapterView parent, View v, int position, long id) {
            if (selectionListener == null) {
                return;
            }

            selectionListener.onDataSelected(parent, v, position, id);

        }

    }

}