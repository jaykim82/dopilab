package com.dopilab.dutchpay;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by SDS on 2016-12-27.
 */
public class CustomEditText extends AppCompatEditText implements TextWatcher, View.OnTouchListener, View.OnFocusChangeListener {

    private Drawable clearIcon, idIcon, passwordIcon;
    private OnFocusChangeListener onFocusChangeListener;
    private OnTouchListener onTouchListener;

    public CustomEditText(final Context context) {
        super(context);
        init(context);
    }

    public CustomEditText(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomEditText(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @Override
    public void setOnFocusChangeListener(OnFocusChangeListener onFocusChangeListener) {
        this.onFocusChangeListener = onFocusChangeListener;
    }

    @Override
    public void setOnTouchListener(OnTouchListener onTouchListener) {
        this.onTouchListener = onTouchListener;
    }

    private void init(Context context) {
        // clear text icon
        this.clearIcon = DrawableCompat.wrap(ContextCompat.getDrawable(context, R.drawable.abc_ic_clear_mtrl_alpha));
        DrawableCompat.setTintList(this.clearIcon,getHintTextColors());

        // edit text ID로 구분하여 추가로 설정할 drawable 제어
        if(this.getResources().getResourceName(this.getId()).contains("login_id")) {
            this.idIcon = DrawableCompat.wrap(ContextCompat.getDrawable(context, R.drawable.login_id_icon));
        } else if(this.getResources().getResourceName(this.getId()).contains("login_password")) {
            this.passwordIcon = DrawableCompat.wrap(ContextCompat.getDrawable(context, R.drawable.login_password_icon));
        }

        this.setCompoundDrawablePadding(20);

        setClearIconVisible(false);

        super.setOnTouchListener(this);
        super.setOnFocusChangeListener(this);
        addTextChangedListener(this);
    }


    @Override
    public void onFocusChange(final View view, final boolean hasFocus) {
        if (hasFocus) {
            setClearIconVisible(getText().length() > 0);
        } else {
            setClearIconVisible(false);
        }

        if (onFocusChangeListener != null) {
            onFocusChangeListener.onFocusChange(view, hasFocus);
        }
    }


    @Override
    public boolean onTouch(final View view, final MotionEvent motionEvent) {
        final int x = (int) motionEvent.getX();
        if (clearIcon.isVisible() && x > getWidth() - getPaddingRight() - clearIcon.getIntrinsicWidth()) {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                setError(null);
                setText(null);
            }
            return true;
        }

        if (onTouchListener != null) {
            return onTouchListener.onTouch(view, motionEvent);
        } else {
            return false;
        }

    }

    @Override
    public final void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
        if (isFocused()) {
            setClearIconVisible(s.length() > 0);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void afterTextChanged(Editable s) {}

    private void setClearIconVisible(boolean visible) {
        clearIcon.setVisible(visible, false);

        if(this.getResources().getResourceName(this.getId()).contains("login_id")) {
            idIcon.setVisible(true, false);
            setCompoundDrawablesWithIntrinsicBounds(idIcon, null, visible ? clearIcon : null, null);
        } else if(this.getResources().getResourceName(this.getId()).contains("login_password")) {
            passwordIcon.setVisible(true, false);
            setCompoundDrawablesWithIntrinsicBounds(passwordIcon, null, visible ? clearIcon : null, null);
        } else {
            setCompoundDrawablesWithIntrinsicBounds(null, null, visible ? clearIcon : null, null);
        }

    }


}

