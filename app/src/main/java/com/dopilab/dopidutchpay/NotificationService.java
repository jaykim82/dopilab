package com.dopilab.dopidutchpay;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

/**
 * Created by 지엽 on 2016-11-10.
 */

public class NotificationService extends NotificationListenerService {
    @Override
    public void onCreate() {
        super.onCreate();
//        Toast.makeText(getApplicationContext(), "notificationlistenerservice oncreate", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        String pack = sbn.getPackageName();
        String ticker = sbn.getNotification().tickerText.toString();
        Bundle extras = sbn.getNotification().extras;
        String title = extras.getString("android.title");
        String text = extras.getCharSequence("android.text").toString();

        Intent msgrcv = new Intent("Msg");
        msgrcv.putExtra("package", pack);
        msgrcv.putExtra("ticker", ticker);
        msgrcv.putExtra("title", title);
        msgrcv.putExtra("text", text);

        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(msgrcv);

    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        Toast.makeText(getApplicationContext(), "Notification removed", Toast.LENGTH_LONG).show();

    }

}
