package com.dopilab.dopidutchpay;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by 지엽 on 2016-10-14.
 */

public class ContactListItemView extends LinearLayout {
    TextView mText01;
    TextView mText02;


    public ContactListItemView(Context context) {
        super(context);
    }

    public ContactListItemView(Context context, ContactListItem item) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.listitem, this, true);

        mText01 = (TextView) findViewById(R.id.listitem_tv_name);
        mText01.setText(item.getData(0));
        mText02 = (TextView) findViewById(R.id.listitem_tv_phone);
        mText02.setText(item.getData(1));
    }

    public void setText(int index, String data) {
        if(index == 0) {
            mText01.setText(data);
        } else if(index == 1) {
            mText02.setText(data);
        } else {
            throw new IllegalArgumentException();
        }
    }

}
