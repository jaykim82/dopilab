package com.dopilab.dopidutchpay;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by 지엽 on 2016-10-23.
 */

public class DisplayCallLog extends AppCompatActivity {
    private final int ID_REQUEST_PERMISSION = 1000;
    TextView mTextView1;
    Button mButton1;
    String mName, mPhone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another);

        mButton1 = (Button) findViewById(R.id.another_btn1);
        mButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity();
            }
        });

        mTextView1 = (TextView) findViewById(R.id.another_tv1);

        Intent intent = getIntent();
        mName = intent.getStringExtra("name");
        mPhone = intent.getStringExtra("phone");

        Cursor cursor = getCallLogCursor(mName, mPhone);
        displayCallLog(cursor, mName, mPhone, mTextView1);

    }

    public Cursor getCallLogCursor(String name, String phone) {
        Cursor cursor = null;
        // CallLog의 전화번호는 "-" 이 빠진 형태로 저장되어 있음
        String phone_onlynumber = phone.replace("-","");
        try {
            cursor = getContentResolver().query(
                    CallLog.Calls.CONTENT_URI,
                    new String[] {
                            CallLog.Calls.NUMBER,
                            CallLog.Calls.TYPE,
                            CallLog.Calls.DATE,
                            CallLog.Calls.DURATION},
                    CallLog.Calls.NUMBER+"=?",
                    new String[]{phone_onlynumber},
                    null);

        } catch(Exception e) {
//            Toast.makeText(getApplicationContext(), "Exception message : " + e.getMessage(), Toast.LENGTH_LONG).show();
            // 권한 관련 exception이 발생한 경우 아래와 같이 처리
            if(e.getMessage().contains("Permission")) {
                // 권한 exception일 때 부족한 권한의 Manifest나 Uri code를 받아오게 해서 requestPermission 하면 좋을 듯

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    int permissionCheck = checkSelfPermission(Manifest.permission.READ_CALL_LOG);
                    if(permissionCheck == PackageManager.PERMISSION_DENIED) {
                        requestPermissions(new String[] {Manifest.permission.READ_CALL_LOG}, ID_REQUEST_PERMISSION);
                    }

                }

            } else {
                e.printStackTrace();
            }
        }

        return cursor;
    }

    public void displayCallLog(Cursor cursor, String name, String phone, TextView textView) {
        String type, date, duration;

        if(cursor.getCount() > 0) {
            type = date = duration = "";
            cursor.moveToFirst();

            while(cursor.moveToNext()) {
                type = cursor.getString(cursor.getColumnIndex(CallLog.Calls.TYPE));
                date = cursor.getString(cursor.getColumnIndex(CallLog.Calls.DATE));
                duration = cursor.getString(cursor.getColumnIndex(CallLog.Calls.DURATION));

//                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

                textView.append("\n----------------------------------------------------------\n" + phone + " / " + name + " / " + type + "\n" + date + " / " + duration);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        Toast.makeText(getApplicationContext(), "permissions = " + permissions[0], Toast.LENGTH_LONG).show();
//        Toast.makeText(getApplicationContext(), "grantResults = " + grantResults[0], Toast.LENGTH_LONG).show();
        if(requestCode == ID_REQUEST_PERMISSION) {
            if(permissions[0].equals(Manifest.permission.READ_CALL_LOG)) {
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    displayCallLog(getCallLogCursor(mName, mPhone), mName, mPhone, mTextView1);

                } else {

                }

            } else {

            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            closeActivity();
            return true;
        }

        return false;
    }

    public void closeActivity() {
        Intent resultIntent = new Intent();
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

}
