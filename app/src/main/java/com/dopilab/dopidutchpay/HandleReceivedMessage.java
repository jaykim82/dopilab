package com.dopilab.dopidutchpay;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by 지엽 on 2016-11-05.
 */

public class HandleReceivedMessage {
//    private static final String PHONE_NUMBER_KB = "01035507235";
//    private static final String PHONE_NUMBER_SAEMAUL = "01035507235";
//    private static final String PHONE_NUMBER_NH = "01035507235";
    private static final String PHONE_NUMBER_KB = "15889999";
    private static final String PHONE_NUMBER_SAEMAUL = "15999000";
    private static final String PHONE_NUMBER_NH = "15882100";
    private static final String PACKAGE_NAME_KB = "com.kbstar.starpush";

    private List<String> phoneNumberList;
    private List<String> packageNameList;

    public String transferer;
    public String transferAmount;

    public HandleReceivedMessage() {
        phoneNumberList = new ArrayList<String>();
        phoneNumberList.add(PHONE_NUMBER_KB);
        phoneNumberList.add(PHONE_NUMBER_SAEMAUL);
        phoneNumberList.add(PHONE_NUMBER_NH);

        packageNameList = new ArrayList<String>();
        packageNameList.add(PACKAGE_NAME_KB);
    }

    public boolean identifyNotificationBank(String packageName, String message) {
        boolean result = false;

        if(packageNameList.contains(packageName) && message.contains("입금")) {
            result = true;
        }

        return result;
    }

    public boolean identifyTextMessageBank(String phoneNumber, String message) {
        boolean result = false;

        if(phoneNumberList.contains(phoneNumber) && message.contains("입금")) {
            result = true;
        }

        return result;
    }

    public void parseTextMessage(String phoneNumber, String message) {

        StringTokenizer st = null;

        switch (phoneNumber) {
//        국민
//        1588-9999
//        [Web발신]
//        [KB국민은행] 09/06 01:50 086601***39557  출금 5,000,000원
            case PHONE_NUMBER_KB :
                break;

//        새마을
//        1599-9000
//        [Web발신]
//        <새마을금고>900320**3 삼성SDS 입금89,500 잔액183,895원 10/26 03:00
            case PHONE_NUMBER_SAEMAUL:
                st = new StringTokenizer(message, " ");
                st.nextToken(); //<새마을금고>900320**3
                transferer = st.nextToken();    //삼성SDS
                transferAmount = st.nextToken().replace("입금", "");  //89,500
                break;

//        농협
//        1588-2100
//        [Web발신]
//        농협 입금31,000원
//        10/28 16:14 079-02-****11 오규삼 잔액533,107원
            case PHONE_NUMBER_NH :
                st = new StringTokenizer(message, " ");
                st.nextToken(); //[Web발신]\n농협
                StringTokenizer st2 = new StringTokenizer(st.nextToken(), "원"); //입금31,000 | 10/28
                transferAmount = st2.nextToken().replace("입금", ""); //31,000
                st.nextToken(); //16:14
                st.nextToken(); //079-02-****11
                transferer = st.nextToken();    //오규삼
                break;

            default:
        }

        if (!transferAmount.contains("원")) {
            transferAmount = transferAmount + "원";
        }


    }


    public void parseNotificationMessage(String packageName, String message) {

        StringTokenizer st = null;

        switch(packageName) {

            case PACKAGE_NAME_KB :
                st = new StringTokenizer(message, " ");
                st.nextToken();
                st.nextToken();
                st.nextToken();
                transferer = st.nextToken();
                st.nextToken();
                transferAmount = st.nextToken();
                break;

            default :
        }

        if (!transferAmount.contains("원")) {
            transferAmount = transferAmount + "원";
        }
    }

    public String getTransferer() {
        return transferer;
    }

    public String getTransferAmount() {
        return transferAmount;
    }

}
