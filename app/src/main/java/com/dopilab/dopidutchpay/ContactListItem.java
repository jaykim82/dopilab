package com.dopilab.dopidutchpay;

/**
 * Created by 지엽 on 2016-10-13.
 */

public class ContactListItem {
    public String[] mData;
    private boolean mSelectable = true;

    public ContactListItem(String[] obj) {
        mData = obj;
    }

    public ContactListItem(String name, String phone) {
        mData = new String[2];

        mData[0] = name;
        mData[1] = phone;
    }

    public boolean isSelectable() {
        return mSelectable;
    }

    public void setSelectable(boolean selectable) {
        mSelectable = selectable;
    }

    public String[] getData() {
        return mData;
    }

    public String getData(int index) {
        if(mData == null || index >= mData.length) {
            return null;
        }

        return mData[index];
    }

    public void setData(String[] obj) {
        mData = obj;
    }

    public int compareTo(ContactListItem other) {
        if (mData != null) {
            String[] otherData = other.getData();
            if (mData.length == otherData.length) {
                for (int i = 0; i < mData.length; i++) {
                    if (!mData[i].equals(otherData[i])) {
                        return -1;
                    }
                }
            } else {
                return -1;
            }
        } else {
            throw new IllegalArgumentException();
        }

        return 0;
    }
}
