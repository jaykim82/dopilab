package com.dopilab.dopidutchpay;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final int ID_REQUEST_PERMISSION = 1000;
    private final int ID_REQUEST_ACTIVITY = 2000;

    List<ContactListItem> contactListOrg;
    ContactListViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar abar = getActionBar();

        // 검색어 입력칸 입니다
        EditText editText = (EditText) findViewById(R.id.main_et1);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                초성이 여러개 입력될 경우 onTextChanged가 이상하게 여러번 호출됨
//                ㅅㄷㅁ 해도 신동민 못 찾음. 즉, 초성의 경우 이름 첫 글자부터 순서가 맞아야 하고,
//                이름 글자를 정확히 입력할 경우 이름 중간의 글자도 검색됨
                String searchKeyword = s.toString();
                List<ContactListItem> contactList = new ArrayList<ContactListItem>();

                if(contactListOrg == null) {
                    contactListOrg = new ArrayList<ContactListItem>();
                }

                if("".equals(searchKeyword)) {  //검색어가 공백이면 전체 보여주기
                    adapter.setListItems(contactListOrg);

                } else {

                    String iniName;
                    ContactListItem item;

                    for(int i = 0; i < contactListOrg.size(); i++) {
                        item = contactListOrg.get(i);
                        iniName = CommonUtils.getHangulInitialSound(item.mData[0], searchKeyword);
                        if (iniName.indexOf(searchKeyword) >= 0) {
                            contactList.add(item);
                        }
                    }

                    adapter.setListItems(contactList);
                }

                adapter.notifyDataSetChanged();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        });

        // Contacts를 가져올 때 권한이나 기타 exception이 있는 경우 위해 try catch 처리
        try {
            contactListOrg = getContactList();
            showListView(contactListOrg);

        } catch(Exception e) {
//            Toast.makeText(getApplicationContext(), "Exception message : " + e.getMessage(), Toast.LENGTH_LONG).show();
            // 권한 관련 exception이 발생한 경우 아래와 같이 처리
            if(e.getMessage().contains("Permission")) {
                // 권한 exception일 때 부족한 권한의 Manifest나 Uri code를 받아오게 해서 requestPermission 하면 좋을 듯

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    List<String> strList = new ArrayList<String>();

                    if(checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_DENIED) {
                        strList.add(Manifest.permission.READ_CONTACTS);
                    }

                    if(checkSelfPermission(Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_DENIED) {
                        strList.add(Manifest.permission.READ_CALL_LOG);
                    }

                    if(checkSelfPermission(Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_DENIED) {
                        strList.add(Manifest.permission.RECEIVE_SMS);
                    }

                    if(strList.size() > 0) {
                        String[] permissions = new String[strList.size()];
                        strList.toArray(permissions);
                        requestPermissions(permissions, ID_REQUEST_PERMISSION);
                    }

                }

            } else {
                e.printStackTrace();
            }
        }

//        long startTime = System.currentTimeMillis();
//        long stopTime = System.currentTimeMillis();
//        long elapsedTime = stopTime - startTime;
//        Log.d("Elapsed Time", "" + elapsedTime);

        // app icon 관련
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // notification 수신 관련
        LocalBroadcastManager.getInstance(this).registerReceiver(onNotice, new IntentFilter("Msg"));
    }

        private BroadcastReceiver onNotice = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
    //            Toast.makeText(getApplicationContext(), intent.getStringExtra("package"), Toast.LENGTH_LONG).show();

                String notiText = intent.getStringExtra("text");
                String notiPackage = intent.getStringExtra("package");
                HandleReceivedMessage handler = new HandleReceivedMessage();

                if(handler.identifyNotificationBank(notiPackage, notiText)) {
                    handler.parseNotificationMessage(notiPackage, notiText);
                    CommonUtils.createDepositNotification(context, handler.getTransferer(), handler.getTransferAmount(), notiPackage, notiText);
                }

            }
        };

    public List<ContactListItem> getContactList() {

        List<ContactListItem> contactList = new ArrayList<ContactListItem>();

        adapter = new ContactListViewAdapter(this);

        // contact Cursor
        Cursor contactCursor = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                        ContactsContract.CommonDataKinds.Phone.TYPE,
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                        ContactsContract.CommonDataKinds.Phone.NUMBER},
                ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER + "=1",
                null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);

        if (contactCursor.getCount() > 0) {

            String id, name, number;
            int type;

            contactCursor.moveToFirst();
            while (contactCursor.moveToNext()) {
                id = name = number = "";
                type = 0;

                id = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                type = contactCursor.getInt(contactCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                name = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                number = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                // type -> 1=HOME, 2=MOBILE
                if (type == 2) {
                    contactList.add(new ContactListItem(name, number));
                }
            }
            contactCursor.close();
        }

        return contactList;

    }

    public void showListView(List<ContactListItem> contactList) {
        ContactListView contactListView = (ContactListView) findViewById(R.id.listView1);
        adapter.setListItems(contactList);
        adapter.notifyDataSetChanged();

        contactListView.setAdapter(adapter);
        contactListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactListItem curItem = (ContactListItem) parent.getAdapter().getItem(position);
                String[] curData = curItem.getData();

                Intent intent = new Intent(getApplicationContext(), DisplayCallLog.class);
                intent.putExtra("name", curData[0]);
                intent.putExtra("phone", curData[1]);
                startActivityForResult(intent, ID_REQUEST_ACTIVITY);

            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == ID_REQUEST_PERMISSION) {
            if(permissions[0].equals(Manifest.permission.READ_CONTACTS)) {
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    contactListOrg = getContactList();
                    showListView(contactListOrg);
                } else {

                }

            } else {

            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == ID_REQUEST_ACTIVITY) {
            if(resultCode == Activity.RESULT_OK) {

            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

}
