package com.dopilab.dopidutchpay;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by 지엽 on 2016-11-25.
 */

public class PracticeActivity extends AppCompatActivity {

    EditText edit01;

    public static final int PROGRESS_DIALOG = 1001;
    ProgressDialog progressDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice);

        ProgressBar proBar = (ProgressBar) findViewById(R.id.pb_loading);
        proBar.setIndeterminate(false);
        proBar.setMax(100);
        proBar.setProgress(80);

        Button btnShow = (Button) findViewById(R.id.btnShow);
        Button btnClose = (Button) findViewById(R.id.btnClose);

        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(PROGRESS_DIALOG);
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);

        View v = menu.findItem(R.id.menu_search).getActionView();
        edit01 = (EditText) v.findViewById(R.id.edit01);

        if (edit01 != null) {
            edit01.setOnEditorActionListener(onSearchListener);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_refresh:
                Toast.makeText(this, "menu refresh", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.menu_search:
                Toast.makeText(this, "menu search", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.menu_settings:
                Toast.makeText(this, "menu settings", Toast.LENGTH_SHORT).show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private TextView.OnEditorActionListener onSearchListener = new TextView.OnEditorActionListener() {
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (event == null || event.getAction() == KeyEvent.ACTION_UP) {
                search();

                InputMethodManager inputManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }

            return (true);
        }
    };

    private void search() {
        String searchString = edit01.getEditableText().toString();
        Toast.makeText(this, "searched : " + searchString, Toast.LENGTH_SHORT).show();
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case (PROGRESS_DIALOG) :
                progressDialog = new ProgressDialog(this);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setMessage("데이터 확인 중");
                return progressDialog;
        }
        return null;
    }

}
